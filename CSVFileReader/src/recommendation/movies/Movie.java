package recommendation.movies;
import java.util.ArrayList;
import java.util.HashSet;

import recommendation.interfaces.Item;

public class Movie implements Comparable<Movie>, Item {
	private final int _MovieId;	
	private final String _title;
	private final int _year;
	private final HashSet<String> _genres;
	private HashSet<Integer> _ratedUser;
	private double _avrating;
	
	public Movie(String movieId, String title, String genres) 
	{
		String[] genereArray = genres.split("\\|");
		_genres = new HashSet<String>();
		
		for(int i = 0; i < genereArray.length; i++)
		{
			if (!genereArray[i].contains("("))
			{
				_genres.add(genereArray[i]);
			}
			else
			{
				
			}
			
		}
		
		_MovieId = Integer.parseInt(movieId);
		
		String columnNameYear = title.replaceAll("%2C", ",");
		
		if (columnNameYear.startsWith("\"") && columnNameYear.endsWith("\""))
		{
			columnNameYear = columnNameYear.substring(1, columnNameYear.length()-1);
		}
		
		columnNameYear.trim();
		
		if (columnNameYear.endsWith(")")&&columnNameYear.charAt(columnNameYear.length()-6)=='(')
		{
			_title = columnNameYear.substring(0, columnNameYear.length()-7).trim();
			_year = Integer.parseInt(columnNameYear.substring(columnNameYear.length()-5,columnNameYear.length()-1));
		} else {
			_title = columnNameYear;
			_year = 0;
		}
		
		_ratedUser = new HashSet<Integer>();
		
	}
	
	public int getId() 
	{
		return this._MovieId;
	}
	
	public int getYear() 
	{
		return this._year;
	}
	
	public HashSet<String> getGenres()
	{
		return new HashSet<String>(_genres);
	}
	
	public void addUserId(int userid)
	{
		_ratedUser.add(userid);
	}
	
	public ArrayList<Integer> getUserId()
	{
		return new ArrayList<Integer>(_ratedUser);
	}
	
	public void setAvRating(double rating)
	{
		this._avrating = rating;
	}
	
	public double getAvRating()
	{
		return this._avrating;
	}


	@Override
	public int compareTo(Movie o) {
		int result;
		result = -(int)((this.getAvRating() - o.getAvRating())*100000);
		
		return result;
	}
	
	@Override
	public String getName() {
		return this._title;
	}
}
