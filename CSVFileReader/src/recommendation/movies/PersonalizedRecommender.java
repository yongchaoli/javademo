package recommendation.movies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


import recommendation.interfaces.IMovieRecommender;
import recommendation.ratings.Rating;

public class PersonalizedRecommender implements IMovieRecommender {

	private List<Movie> _movies;
	private HashMap<Integer, Rating> _ratings;
	private HashMap<Integer, Double> _MovieRating;
	private HashSet<Integer> _userIdSet;
	private HashSet<String> _genres;
	//private HashMap<Integer, Integer> mostSimilarUsers;
	private HashMap<Integer, Integer> _RankAndMovieId;
	
	
	@Override
	public ArrayList<Movie> recommend(int userid, int recNum) 
	{
		int simId = CalculateSimilar(userid);
		ArrayList<Movie> recMovies = new ArrayList<Movie>();
		
		//if (mostSimilarUsers.get(userid) != 0)
		if (simId != 0)
		{
			//Rating similarRating = this._ratings.get(mostSimilarUsers.get(userid));
			Rating similarRating = this._ratings.get(simId);
			Rating inputRating = this._ratings.get(userid);
			
			for( int k : similarRating.getMovieRate().keySet())
			{
				if (!inputRating.getMovieRate().containsKey(k))
				{
					recMovies.add(_movies.get(_RankAndMovieId.get(k)));
				}
				if (recMovies.size() == recNum)
				{
					break;
				}
			}
		}
		
		Collections.sort(recMovies);
		
		return recMovies;
	}

	@Override
	public ArrayList<Movie> recommend(int userid, int recNum, String genre) 
	{
		CalculateSimilar(userid);
		ArrayList<Movie> recMovies = new ArrayList<Movie>();
		
		ArrayList<String> forCompare = new ArrayList<String>();
		
		forCompare.add(genre);
		
		for (int m = 0; m < this._movies.size(); m++)
		{
			if (!this._movies.get(m).getUserId().contains(userid) && this._movies.get(m).getGenres().containsAll(forCompare))
			{
				recMovies.add(_movies.get(m));
				recNum--;
			}
				
			if (recNum == 0)
			{
				break;
			}
		}

		Collections.sort(recMovies);
		
		return recMovies;	
	}

	public PersonalizedRecommender(List<Movie> movies, ArrayList<Rating> ratings) 
	{
		_movies = new ArrayList<Movie>(movies);
		
		this._ratings = new HashMap<Integer, Rating>();
		
		for(int i = 0; i < ratings.size(); i++)
		{
			_ratings.put(ratings.get(i).getUserId(), ratings.get(i));
		}
						
		_MovieRating = new HashMap<Integer, Double>();
		
		CalculateAvRating();
		
		Collections.sort(_movies);
		
//		for (Movie movie : _movies) 
//		{
//			System.out.println(movie.getId() + " " + movie.getAvRating()  + " " + movie.getUserId());
//		}
		
		_RankAndMovieId = new HashMap<Integer, Integer>();
		
		for(int m = 0; m < this._movies.size(); m++)
		{
			_RankAndMovieId.put(this._movies.get(m).getId(), m);
		}
		
		UserIdInit();
		
		//CalculateSimilar();
	}
	
	private void UserIdInit() {
		
		_userIdSet = new HashSet<Integer>();
		
		for (int k : this._ratings.keySet())
		{
			_userIdSet.add(this._ratings.get(k).getUserId());
		}
		
	}

	private void CalculateAvRating() 
	{
		HashMap<Integer, Double> ratingdata;
		this._genres = new HashSet<String>();
		
		for (Map.Entry<Integer, Rating> rating : _ratings.entrySet())
		{
			ratingdata = new HashMap<Integer, Double>(rating.getValue().getMovieRate());
			
			for (int m = 0; m < _movies.size(); m++ )
			{
				if (ratingdata.containsKey(_movies.get(m).getId()))
				{
					
					_movies.get(m).addUserId(rating.getValue().getUserId());
					if (!_MovieRating.containsKey(_movies.get(m).getId()))
					{
						
						_MovieRating.put(_movies.get(m).getId(), ratingdata.get(_movies.get(m).getId()));
					}
					else
					{
						_MovieRating.put(_movies.get(m).getId(), ratingdata.get(_movies.get(m).getId()) + _MovieRating.get(_movies.get(m).getId()));
					}
				}
			}
		}
		
		for (int m = 0; m < _movies.size(); m++ )
		{
			if (_MovieRating.containsKey(_movies.get(m).getId()))
			{
				_movies.get(m).setAvRating(_MovieRating.get(_movies.get(m).getId())/_movies.get(m).getUserId().size());
			}			
			
			this._genres.addAll(_movies.get(m).getGenres());
		}
		
	}
	
	public HashSet<String> getGenres() 
	{
		return new HashSet<String>(this._genres);
	}
	
	private int CalculateSimilar(int userid) 
	{
		//mostSimilarUsers = new HashMap<Integer, Integer>();
		HashMap<Integer, Double> user, ompareUser;
		
		double cumSimilar, maxCum;
		int simId;
		
		//int count = 0, total = this._ratings.size();
		 
		//System.out.print("Preparing: ");
		//for(Map.Entry<Integer, Rating> userA : this._ratings.entrySet())
		//{
			
//			count++;
//			
//			if (count % 100 == 0)
//			{
//				int output = (int)(100*count/total);
//				System.out.print(output + "% ");
//			}
		
			
			maxCum = 0;
			
			simId = 0;
			
			for (Map.Entry<Integer, Rating> userB : this._ratings.entrySet())
			{
				//if (userB.getKey() == userA.getKey())
				if (userB.getKey() == userid)
				{
					continue;
				}
				
				
				cumSimilar = 0;
				
				//user = userA.getValue().getMovieRate();
				user = _ratings.get(userid).getMovieRate();
				ompareUser = userB.getValue().getMovieRate();
				
				for (Map.Entry<Integer, Double> moviedata : user.entrySet())
				{
					if (ompareUser.containsKey(moviedata.getKey()))
					{
						cumSimilar += (moviedata.getValue() - 2.5) * (ompareUser.get((moviedata.getKey())) - 2.5);
					}
				}
				
				if (cumSimilar > maxCum)
				{
					maxCum = cumSimilar;
					simId = userB.getKey();
				}
				
			}
			
			//System.out.println(userA.getKey() + " " + simId + " " + maxCum);
			//mostSimilarUsers.put(userA.getKey(), simId);
			
		//}
//		System.out.println("100%");
		return simId;
	}
}
