package recommendation.movies;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


import recommendation.interfaces.IMovieRecommender;
import recommendation.ratings.Rating;

public class PopularRecommender implements IMovieRecommender {
	private List<Movie> _movies;
	private HashMap<Integer, Rating> _ratings;
	private HashMap<Integer, Double> _MovieRating;
	private HashSet<Integer> _userIdSet;
	private HashSet<String> _genres;
	
	public PopularRecommender(List<Movie> movies, ArrayList<Rating> ratings)
	{
		_movies = new ArrayList<Movie>(movies);
		
		this._ratings = new HashMap<Integer, Rating>();
		
		for(int i = 0; i < ratings.size(); i++)
		{
			_ratings.put(ratings.get(i).getUserId(), ratings.get(i));
		}
		
		_MovieRating = new HashMap<Integer, Double>();
		
		CalculateAvRating();
		
		Collections.sort(_movies);
		
		
//		for (Movie movie : _movies) 
//		{
//			System.out.println(movie.getId() + " " + movie.getAvRating()  + " " + movie.getUserId());
//		}
		
		
		UserIdInit();

	}
	
	public ArrayList<Movie> getMovies()
	{
		ArrayList<Movie> rl = new ArrayList<Movie>(this._movies);
		return rl;
	}

	
	private void UserIdInit() {
		
		_userIdSet = new HashSet<Integer>();
		
		for (Map.Entry<Integer, Rating> rating : this._ratings.entrySet())
		{
			_userIdSet.add(rating.getValue().getUserId());
		}
		
	}

	private void CalculateAvRating() 
	{
		HashMap<Integer, Double> ratingdata;
		this._genres = new HashSet<String>();
		
		for (Map.Entry<Integer, Rating> rating : _ratings.entrySet())
		{
			ratingdata = new HashMap<Integer, Double>(rating.getValue().getMovieRate());
			
			for (int m = 0; m < _movies.size(); m++ )
			{
				if (ratingdata.containsKey(_movies.get(m).getId()))
				{
					
					_movies.get(m).addUserId(rating.getValue().getUserId());
					if (!_MovieRating.containsKey(_movies.get(m).getId()))
					{
						
						_MovieRating.put(_movies.get(m).getId(), ratingdata.get(_movies.get(m).getId()));
					}
					else
					{
						_MovieRating.put(_movies.get(m).getId(), ratingdata.get(_movies.get(m).getId()) + _MovieRating.get(_movies.get(m).getId()));
					}
				}
			}
		}
		
		for (int m = 0; m < _movies.size(); m++ )
		{
			if (_MovieRating.containsKey(_movies.get(m).getId()))
			{
				_movies.get(m).setAvRating(_MovieRating.get(_movies.get(m).getId())/_movies.get(m).getUserId().size());
			}			
			
			this._genres.addAll(_movies.get(m).getGenres());
		}
		
	}
	
	@Override
	public ArrayList<Movie> recommend(int userid, int recNum) 
	{
		ArrayList<Movie> recMovies = new ArrayList<Movie>();
		
		if (this._userIdSet.contains(userid)) 
		{
			for (int m = 0; m < this._movies.size(); m++)
			{
				if (!this._movies.get(m).getUserId().contains(userid))
				{
					recMovies.add(_movies.get(m));
					recNum--;
				}
				
				if (recNum == 0)
				{
					break;
				}
			}

		}
		
		return recMovies;
	}
	
	public ArrayList<Movie> recommend(int recNum, String genres) 
	{
		ArrayList<Movie> recMovies = new ArrayList<Movie>();
		
		ArrayList<String> forCompare = new ArrayList<String>();
		
		forCompare.add(genres);
		
		for (int m = 0; m < this._movies.size(); m++)
		{
			if (this._movies.get(m).getGenres().containsAll(forCompare))
			{
				recMovies.add(_movies.get(m));
				recNum--;
			}
				
			if (recNum == 0)
			{
				break;
			}
		}

	
		
		return recMovies;
	}
	
	public HashSet<String> getGenres() 
	{
		return new HashSet<String>(this._genres);
	}

	


	@Override
	public ArrayList<Movie> recommend(int userid, int recNum, String genre) {
		
		ArrayList<Movie> recMovies = new ArrayList<Movie>();
		
		ArrayList<String> forCompare = new ArrayList<String>();
		
		forCompare.add(genre);
		
		for (int m = 0; m < this._movies.size(); m++)
		{
			if (!this._movies.get(m).getUserId().contains(userid) && this._movies.get(m).getGenres().containsAll(forCompare))
			{
				recMovies.add(_movies.get(m));
				recNum--;
			}
				
			if (recNum == 0)
			{
				break;
			}
		}

	
		
		return recMovies;
		
		
	}
}
