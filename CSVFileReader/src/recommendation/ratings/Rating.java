package recommendation.ratings;
import java.util.*;


public class Rating {
	private final int _userId;
	private HashMap<Integer, Double> _movieRate;
	
	
	public Rating(String userId) 
	{
		_userId = Integer.parseInt(userId);
		
		_movieRate = new HashMap<Integer, Double>();
				
	}
	
	public void AddMovieRate(String movieId, String rate) 
	{
		this._movieRate.put(Integer.parseInt(movieId), Double.parseDouble(rate));
	}
	
	public HashMap<Integer, Double> getMovieRate()
	{
		HashMap<Integer, Double> r = new HashMap<Integer, Double>(this._movieRate);
		return r;
	}
	
	public int getUserId ()
	{
		return this._userId;
	}
}
