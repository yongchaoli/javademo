/**
 * 
 */
package recommendation.interfaces;


/**
 * @author Camillia Elachqar
 * @version Phase1
 * @since 2018-09-25
 *
 */
public interface Item {
	public String getName();
}
