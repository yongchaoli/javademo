/**
 * 
 */
package recommendation.interfaces;

import recommendation.movies.Movie;
import java.util.*;

/**
 * @author Hua
 *
 */
public interface IMovieRecommender {
	
	ArrayList<Movie> recommend (int userid, int recNum);
	ArrayList<Movie> recommend (int userid, int recNum, String genre);
}
