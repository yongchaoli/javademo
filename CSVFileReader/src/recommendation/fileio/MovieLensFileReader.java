package recommendation.fileio;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;



public class MovieLensFileReader {
	
	final public String[] columnNames;
	final public String[][] dataList;
	
	public MovieLensFileReader (String fileName) throws IOException
	{
		Path p = Paths.get(fileName);
		List<String> lines;
		lines = Files.readAllLines(p);
		
		if (lines==null || lines.size() == 1) {
			throw new IOException("File is empty");
		}		
		
		String line = lines.get(0);
		String[] temp = line.split(",");		
		
		if (temp.length == 0 || temp == null) {
			throw new IOException("Top line tempelate is not corrected.");
		}
		
		columnNames = new String[temp.length];
		
		for (int i = 0; i < temp.length; i++)
		{
			columnNames[i] = temp[i];
		}
		
		dataList = new String[lines.size()-1][temp.length];
		
		
		for (int i = 0; i < lines.size() - 1; i++)
		{
			line = lines.get(i + 1);
			temp = line.split(",");
			
			if (temp.length == columnNames.length)
			{
				for (int j = 0; j < temp.length; j++)
				{
					dataList[i][j] = temp[j];
				}
				
			}
			else
			{
				System.out.println("No." + (i + 2) + " data error.");
			}

		}
	}
}
