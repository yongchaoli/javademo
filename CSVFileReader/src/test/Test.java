package test;
import java.io.IOException;
import java.util.*;

import recommendation.fileio.MovieLensFileReader;
import recommendation.interfaces.IMovieRecommender;
import recommendation.movies.Movie;
import recommendation.movies.PersonalizedRecommender;
import recommendation.movies.PopularRecommender;
import recommendation.ratings.Rating;

public class Test {

	
	public static void main(String[] args) {
		
		
		
		MovieLensFileReader moviesLoad = null, ratingsLoad = null;
		try
		{
			moviesLoad = new MovieLensFileReader("datafiles\\movies.csv");
			ratingsLoad = new MovieLensFileReader("datafiles\\ratings.csv");
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}

		List<Movie> movies = new ArrayList<Movie>();
		
		
		for (int i = 0; i < moviesLoad.dataList.length; i++)
		{		
			movies.add(new Movie(moviesLoad.dataList[i][0], moviesLoad.dataList[i][1], moviesLoad.dataList[i][2]));			
		}
		
		ArrayList<Rating> ratings = new ArrayList<Rating>();
		
		for (int i = 0, preUserId = 0; i < ratingsLoad.dataList.length; i++)
		{
			if (preUserId != Integer.parseInt(ratingsLoad.dataList[i][0]))
			{
				ratings.add(new Rating(ratingsLoad.dataList[i][0]));
				preUserId = Integer.parseInt(ratingsLoad.dataList[i][0]);
			}
			
			ratings.get(ratings.size()-1).AddMovieRate(ratingsLoad.dataList[i][1], ratingsLoad.dataList[i][2]);
		}
		
		
		ArrayList<Movie> prResult;


		IMovieRecommender ir;
		
		PopularRecommender pr = new PopularRecommender(movies, ratings);
		PersonalizedRecommender prr = new PersonalizedRecommender(movies, ratings);
		
		int userInput = 1,userId,num,prChoice;
		Scanner reader = new Scanner(System.in);
		while(userInput == 1)
		{
			
			
			if (userInput == 1)
			{
				System.out.print("Do you want to user Popular recommnder(1) or Personalized Recommender(2)? ");
				
				prChoice = reader.nextInt();
				
				switch (prChoice)
				{
					case 1:
						ir = pr;
						break;
					case 2:
						ir = prr;
						break;
					default:
						ir = pr;
						break;
				}
				
				
				System.out.print("Input userid: ");
				userId = reader.nextInt();
				System.out.print("Number of movies: ");
				num = reader.nextInt();
				
				prResult = ir.recommend(userId, num);
				
				if (prResult.size() == 0)
				{
					System.out.println("Nothing to tell.");
				}
				else
				{
					for (int i = 0; i < prResult.size(); i++)
					{
						System.out.print(prResult.get(i).getId() + " " + prResult.get(i).getName() + " " + prResult.get(i).getGenres()  + " " + prResult.get(i).getAvRating());
						System.out.println(",\tUser IDs: " + prResult.get(i).getUserId());
						//System.out.println((i + 1) + " " + prResult.get(i).getName());
					}	
					
				}
			}
			System.out.print("Do you want to Continue? Yes(1)/No(0)");
			userInput = reader.nextInt();
		}
		
		
		reader.close();
		
		

	}

}
